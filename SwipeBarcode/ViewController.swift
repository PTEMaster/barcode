//
//  ViewController.swift
//  SwipeBarcode
//
//  Created by mac on 12/08/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var txtStudentId: UITextField!
    @IBOutlet weak var viewbg: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgbarCode: UIImageView!
    @IBOutlet weak var imgQrcode: UIImageView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if let studentid =  UserDefaults.standard.string(forKey: "StudentId") {
            txtStudentId.text = studentid
            txtStudentId.isUserInteractionEnabled = false
            imgbarCode.image = generateBarcode(from: txtStudentId.text!, nameBarCode: "CICode128BarcodeGenerator")
            imgQrcode.image = generateBarcode(from: txtStudentId.text!, nameBarCode: "CIQRCodeGenerator")
             btnSubmit.isHidden = true
        } else {
          txtStudentId.becomeFirstResponder()
        }
        txtStudentId.delegate = self
   //  CIQRCodeGenerator
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        UserDefaults.standard.set(txtStudentId.text!, forKey: "StudentId")
       txtStudentId.resignFirstResponder()
        imgbarCode.image = generateBarcode(from: txtStudentId.text!, nameBarCode: "CICode128BarcodeGenerator")
        imgQrcode.image = generateBarcode(from: txtStudentId.text!, nameBarCode: "CIQRCodeGenerator")
        txtStudentId.isUserInteractionEnabled = false
        btnSubmit.isHidden = true
      //  CICode128BarcodeGenerator
    }
    
    func generateBarcode(from string: String , nameBarCode:String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: nameBarCode) {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
}


extension ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
